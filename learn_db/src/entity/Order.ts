import { Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { OrderItem } from "./OrderItem";
import { User } from "./User";

@Entity()
export class Order {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    total: number;

    @Column()
    qty: number;

    @CreateDateColumn()
    created: Date

    @UpdateDateColumn()
    update: Date

    @OneToMany(() => OrderItem, (OrderItem) => OrderItem.order)
    orderItems: OrderItem[];

    @ManyToOne(() => User, (user) => user.orders)
    user: User;

}