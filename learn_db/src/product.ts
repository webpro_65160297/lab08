import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product";
import { Type } from "./entity/Type";
import { User } from "./entity/User"
import { Role } from "./entity/role";

AppDataSource.initialize().then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    const beverageType = await typesRepository.findOneBy({ id: 1 });
    const bakeryType = await typesRepository.findOneBy({ id: 2 });
    const foodType = await typesRepository.findOneBy({ id: 3 })
    const productsRepository = AppDataSource.getRepository(Product);
    await productsRepository.clear();

    var product = new Product();
    product.id = 1;
    product.name = "Americano";
    product.price = 40;
    product.type = beverageType
    await productsRepository.save(product);

    var product = new Product();
    product.id = 2;
    product.name = "Moccha";
    product.price = 50;
    product.type = beverageType
    await productsRepository.save(product);

    var product = new Product();
    product.id = 3;
    product.name = "Chocolate Signature Cake";
    product.price = 120;
    product.type = bakeryType
    await productsRepository.save(product);

    const products = await productsRepository.find({ relations: { type: true }, });
    console.log(products);



}).catch(error => console.log(error))
