import { AppDataSource } from "./data-source"
import { Type } from "./entity/Type";
import { User } from "./entity/User"
import { Role } from "./entity/role";

AppDataSource.initialize().then(async () => {
    const typesRepository = AppDataSource.getRepository(Type);
    await typesRepository.clear();

    var type = new Type();
    type.id = 1;
    type.name = "beverage";
    await typesRepository.save(type);

    var type = new Type();
    type.id = 2;
    type.name = "bakery";
    await typesRepository.save(type);

    var type = new Type();
    type.id = 3;
    type.name = "food";
    await typesRepository.save(type);
    const types = await typesRepository.find({ order: { id: "ASC" } });
    console.log(types);



}).catch(error => console.log(error))
